class Client:
    def __init__(self, buget):
        self.buget = buget

    # Capitolul 2: Op si structuri de control
    def plateste(self, de_achitat):
        if de_achitat <= self.buget:
            # Scadem din buget
            self.buget -= de_achitat
            return True
        else:
            return False
