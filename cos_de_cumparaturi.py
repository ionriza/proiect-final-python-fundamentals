class CosDeCumparaturi:
    def __init__(self, nume):
        self.nume_bon = nume
        self.lista_de_produse = []

    # Captiolul 5: Lucrul cu fisiere
    def elibereaza_bon(self):
        # parcurgem lista de produse
        # deschidem un fisier pentru scriere
        pret_total = 0
        bon_fiscal = open(f'bon_fiscal_{self.nume_bon}.txt', 'w')
        bon_fiscal.write('BON FISCAL\n')
        bon_fiscal.write('='*32+'\n')
        for produs in self.lista_de_produse:
            bon_fiscal.write(f'{produs.nume},{produs.stoc},{produs.pret * produs.stoc}\n')
            pret_total += produs.pret * produs.stoc
        bon_fiscal.write('='*32+'\n')
        bon_fiscal.write(f'PRET TOTAL,{pret_total}')
        bon_fiscal.close()

    # Captiolul 3: Tipuri avansate de date
    def adauga_produs(self, produs):
        self.lista_de_produse.append(produs)
