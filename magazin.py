# Implementarea unui sistem de gestionare al operatiilor
# dintr-un magazin alimentar
import os
# Avem 3 clase (in 3 fisiere)
# - Client
# - CosDeCumparaturi
# - Produs
from client import Client
from cos_de_cumparaturi import CosDeCumparaturi
from produs import Produs

# Citim dintr-un fisier 'stocuri.txt', de forma
# <nume_produs>,<stoc>,<pret>

# Capitolul 5 - Lucrul cu fisiere
fisier_stocuri = open('stocuri.txt')
produse = fisier_stocuri.read().splitlines()

# Capitolul 3 - Tipuri avansate de date
stoc_magazin = dict() # {}

# Parcurgem lista cu produse citita din 'stocuri.txt'
# si stocam in dictionarul 'stoc_magazin', sub forma urmatoare:
# {'nume_produs': {'cantitate': <cantitate>, 'pret': <pret>}}
for produs in produse:
    nume_produs, cantitate, pret = produs.split(',')
    stoc_magazin[nume_produs] = {'cantitate': int(cantitate), 'pret': int(pret)}

fisier_stocuri.close()
# puteam face si o lista cu alte liste (sau o lista cu tupluri)
# [[paine, 50, 3], [apa, 25, 5]]
# [(paine, 50, 3), (apa, 50, 3)]

# Capitolul 6 - Module in Python
# Citim fisierele din folderul 'liste_cumparaturi'
for fisier in os.listdir('liste_cumparaturi'):
    nume = fisier.split("_")[-1]
    lista_client = open(f'liste_cumparaturi/{fisier}')
    buget = int(lista_client.readline())
    produse = lista_client.read().splitlines()
    client_nou = Client(buget)
    cos_de_cumparaturi_nou = CosDeCumparaturi(nume)
    # parcurgem fiecare produs de pe lista de cumparaturi
    for produs_cantitate in produse:
        nume_produs, cantitate = produs_cantitate.split(',')
        # a clientului si verificam:
        # 1. Daca produsul se afla in stocul nostru
        if stoc_magazin.get(nume_produs):
            # 2. Daca clientul si-l permite (in functie de buget)
            # cantiatea pe care si-o doreste inmultit cu pretul
            pret_total_produs = stoc_magazin[nume_produs]['pret'] * int(cantitate)
            if client_nou.plateste(pret_total_produs):
                # 3. Daca cantitatea din stocul nostru
                if stoc_magazin[nume_produs]['cantitate'] >= int(cantitate):
                    produs_nou = Produs(nume_produs, stoc_magazin[nume_produs]['pret'], int(cantitate))
                    cos_de_cumparaturi_nou.adauga_produs(produs_nou)
    cos_de_cumparaturi_nou.elibereaza_bon()


